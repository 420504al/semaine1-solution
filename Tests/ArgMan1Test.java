import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author David Giasson <david.giasson@claurendeau.qc.ca>
 */
public class ArgMan1Test {
    
    private final String schema = "t$,e#?,s*";
    private final String sampleArgs = "-t -e 5 -s hello";
    
    private ArgMan argman;
    
    @Before
    public void setUp () throws Exception {
        argman = new ArgManSolution(schema);
    }

    @Test
    public void testAllArguments () throws Exception {
        loadArgs(sampleArgs);
        verify(true, 5, "hello");
    }
    
    @Test
    public void testOrderInvariant () throws Exception {
        loadArgs("-s world -e 6 -t");
        verify(true, 6, "world");
    }
    
    @Test
    public void testExplicitlyOptional () throws Exception {
        loadArgs("-t -s hello");
        verify(true, 0, "hello");
    }
    
    @Test
    public void testImplicitlyOptional () throws Exception {
        loadArgs("-e 7 -s world");
        verify(false, 7, "world");
    }
    
    @Test
    public void testMinimumArguments () throws Exception {
        loadArgs("-s hello");
        verify(false, 0, "hello");
    }
    
    @Test (expected = Exception.class)
    public void testMissingArgument () throws Exception {
        loadArgs("-t -e 5");
    }
    
    @Test (expected = Exception.class)
    public void testMissingValue () throws Exception {
        loadArgs("-t -e -s world");
    }
    
    @Test (expected = Exception.class)
    public void testValueConfusion () throws Exception {
        loadArgs("-s -t");
    }
    
    @Test (expected = Exception.class)
    public void testUnexpectedValue () throws Exception {
        loadArgs("-t value -s world");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidValueType () throws Exception {
        loadArgs("-e textv -s world ");
    }
    
    @Test (expected = Exception.class)
    public void testUnknownArgument () throws Exception {
        loadArgs("-x hello -s world");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidSyntax () throws Exception {
        loadArgs("-shello world");
    }
    
    @Test (expected = Exception.class)
    public void testNoArgsLoaded () throws Exception {
        // loadArgs is intentionally left out
        argman.getString('s');
    }
    
    @Test (expected = Exception.class)
    public void testUnknownParameter () throws Exception {
        loadArgs(sampleArgs);
        argman.getString('x');
    }
    
    @Test (expected = Exception.class)
    public void testQueryWrongType () throws Exception {
        loadArgs(sampleArgs);
        argman.getString('e');
    }
    
    @Test
    public void testArgsReloaded () throws Exception {
        // this test is optional (success is preferable but failure is acceptable)
        loadArgs(sampleArgs);
        loadArgs("-e 6 -s world");
        verify(false, 6, "world");
    }
    
    @Test
    public void testFail() {
        fail();
    }
    
    private void loadArgs (String rawArgs) throws Exception {
        argman.loadArgs(rawArgs.split(" "));
    }
    
    private void verify (boolean t, int e, String s) throws Exception {
        assertEquals(t, argman.getBoolean('t'));
        assertEquals(e, argman.getInt('e'));
        assertEquals(s, argman.getString('s'));
    }
}
