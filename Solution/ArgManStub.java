import java.util.Arrays;

public final class ArgManStub implements ArgMan {

    private String[] params;
    private String[] args;

    public ArgManStub (String schema) {
        System.out.println("Schema recu: " + schema);
        
        // Ceci est un squelette de "chargement" du schéma... Mais vous voudrez sûrement
        // l'améliorer un peu dans votre solution à vous. Entre-autres, il manque le support 
        // pour les paramètres optionnels ainsi qu'une meilleure gestion des erreurs.
        this.params = schema.split(",");
        for (String param : this.params) {
            char identifier = param.charAt(0);
            String type = "inconnu";
            switch (param.charAt(1)) {
                case '$' :
                    type = "booleen";
                    break;
                case '#':
                    type = "entier";
                    break;
                case '*':
                    type = "chaine";
                    break;
            }
            System.out.println(" -> Parametre " + identifier + " de type " + type);
        }
    }

    public ArgManStub (String schema, String[] args) {
        this(schema);
        loadArgs(args);
    }

    @Override
    public void loadArgs (String[] args) {
        this.args = args;
        System.out.println("Arguments recus: " + Arrays.toString(args));

        // Après avoir reçu le schéma des paramètres, le ArgMan effectue les diverses actions  
        // requises pour pouvoir par la suite retourner les valeurs des arguments reçus.
        // (Ici par contre, c'est une classe "Stub", donc on ne fait rien de vraiment utile)
        for (String arg : this.args) {
            if (arg.charAt(0) == '-') {
                char argId = arg.charAt(1);
                boolean unknown = true;
                for (String param : this.params) {
                    if (argId == param.charAt(0)) {
                        unknown = false;
                        System.out.println(" -> Argument " + argId + " present");
                    }
                }
                if (unknown) {
                    System.out.println(" -> Argument " + argId + " inconnu");
                }
            }
        }
    }

    @Override
    public int getInt (char paramName) {
        return 42;
    }

    @Override
    public boolean getBoolean (char paramName) {
        return true;
    }

    @Override
    public String getString (char paramName) {
        return "hello";
    }
}
